import pandas as pd
from cardmarket.util import get_market_scheme, save_csv

# following: lambda helper methods for nested data extraction
getComment = lambda comment: comment.find("span").text if comment is not None else ""

get_seller_type = lambda type: type.find("span", {"class": "icon"})['title'] if type is not None else pd.NA

is_special = lambda status: status['title'] if status is not None else pd.NA

def market_by_card(data):
    market_data = get_market_scheme()
    market_rows = list()

    for id, timestamp, html_src in data:
        table_rows = html_src.find("section", {"id": "table"}).find_all("div", {"class": "row no-gutters article-row"})

        for row in table_rows:
            # scrape relevant data from sales market table like seller info, country of location, ...
            sale = row.find("div", {"class": "col-sellerProductInfo"})
            seller_name = sale.find("span", {"class": "seller-name"}).find("a").text
            seller_type = get_seller_type(sale.find("span", {"class": "d-flex has-content-centered me-1 proFaded"}))
            sales = sale.find("span", {"class": "badge"}).text
            country = sale.find("span", {"class": "icon d-flex has-content-centered me-1"})['title'].split(' ')[1]
            condition = sale.find("a", {"class": "article-condition"}).text
            language = sale.find("span", {"class", "icon me-2"})['title']
            attributes = is_special(sale.find("span", {"class": "icon st_SpecialIcon mr-1"}))
            description = sale.find("div", {"class": "col-product"}).find("div", {"class": "product-comments"})
            description = getComment(description)
            offer = float(row.find("div", {"class": "price-container"}).text[:-2].replace(",", "."))
            amount = row.find("div", {"class": "amount-container"}).text
            
            entry = (id, timestamp, sales, country, seller_name, seller_type, condition, language, attributes, description, offer, amount)
            market_rows.append(entry)
    
    # retrieve market data as table
    market_data = pd.DataFrame.from_records(market_rows)
    market_data.columns = ['ID', 'Zeitstempel', 'Bewertungen', 'Land', 'Verkäufername', 'Verkäuferart', 'Zustand', 'Sprache', 'Besonderheiten', 'Beschreibung', 'Preis', 'Anzahl']
    return market_data

def main(data):
    run(data)

def run(data):
    market = market_by_card(data)
    save_csv(market, "market")

if __name__ == "__main__":
    main()