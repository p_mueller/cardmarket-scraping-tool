import pandas as pd
from bs4 import BeautifulSoup
import requests
from datetime import datetime
import time
import os
from http.client import responses

# retrieve subpath of where data is contained
def get_subpath():
    subpath = "data"
    return subpath

# returns today's date
def get_date():
    return datetime.now().date().strftime("%Y-%m-%d")

# returns current timestamp
def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# saves df_table export to csv file
def save_csv(table, name):
    date = get_date()
    datefile = f"{get_subpath()}/{date}/{date}_{name}.csv"

    if not date_exists(date, name):
        if not os.path.exists(f"{get_subpath()}/{date}"):
            os.makedirs(f"{get_subpath()}/{date}")
    table.to_csv(datefile)

# checks if file with name already exists
def date_exists(date, name):
    file_path = os.path.join(f"{get_subpath()}/{date}", f"{date}_{name}.csv")
    if os.path.exists(file_path):
        return True
    return False

def get_price_scheme():
    price_cols = ['ID', 'Zeitstempel', 'Min', 'Trend', '1-Tag-AVG', '7-Tage-AVG', '30-Tage-AVG']
    price_data = pd.DataFrame(columns=price_cols)

    return price_data.copy()

def get_market_scheme():
    market_cols = ['ID', 'Zeitstempel', 'Bewertungen', 'Land', 'Verkäufername', 'Verkäuferart', 'Zustand', 'Sprache', 'Besonderheiten', 'Beschreibung', 'Preis', 'Anzahl']
    market_data = pd.DataFrame(columns=market_cols)

    return market_data.copy()

# retrieves relevant data from excel sheet containing tcg data
def get_excel_sheet():
    # Pfad zur Excel-Datei
    file_path = "XY.xlsx"

    # Lies das gewünschte Arbeitsblatt in ein Pandas DataFrame ein
    df = pd.read_excel(file_path)[['ID', 'Name', 'Nr.', 'Set', 'URL', 'Anzahl', '1-Edition', 'Seltenheit']]

    df = df.loc[df["URL"].drop(0).dropna().index]
    df['Anzahl'] = df['Anzahl'].astype(int)
    df = df.reset_index(drop=True)

    return df

def get_column(df, col_name):
    if df is None:
        df = get_excel_sheet()

    if col_name in df.columns:
        return df[col_name].dropna().tolist()
    
    return None

def get_urls(df=None):
    return get_column(df, 'URL')

def get_ids(df=None):
    return get_column(df, 'ID')

def collect(endswith, target, subpath="data"):
    for root, dirs, files in os.walk(subpath):
        for file in files:
            if file.endswith(endswith):
                file_path = os.path.join(root, file)
                source = pd.read_csv(file_path, index_col=0)
                target = pd.concat([target, source])

    target = target.reset_index(drop=True).sort_values('Zeitstempel')
    target.to_csv(f"{subpath}/{endswith[1:]}")

# retrieves all responses including html src
def get_responses(url_list=None, attempts=10, timeout=5.0):
    id_list = get_ids()

    if url_list is None:
        url_list = get_urls()

    response_list = []
    count = 1

    for url, id in zip(url_list, id_list):
        progress = int(round(count/len(id_list) * 100, 0))

        header = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        response = requests.get(url, headers=header)
        status_code = response.status_code
        print(f"{count}/{len(id_list)} [{progress}%]: [{response.status_code} {responses[status_code]}] [{get_timestamp()}]")
        if response.status_code == 200:
            value = BeautifulSoup(response.content, "html.parser")
            response_list.append((id, get_timestamp(), value))
        else:
            # timeout and try again max. 10 trials
            for i in range(0, attempts):
                time.sleep(timeout)
                response = requests.get(url, headers=header)
                status_code = response.status_code
                print(f"{count}/{len(id_list)} [{progress}%]: [{response.status_code} {responses[status_code]}] [{get_timestamp()}] [RETRY]")
                if response.status_code == 200:
                    try_again_value = BeautifulSoup(response.content, "html.parser")
                    response_list.append((id, get_timestamp(), try_again_value))
                    break

        time.sleep(3.0)
        count += 1

    return response_list

# convert price from str spec format to float
def convert_price(price):
    value = price.text.strip()
    value = value[:-2].replace(',', '.')
    value = float(value)
    
    return value

# searches html formatted table for spec tags
def get_value_by_term(tag, dt_elements):
    try:
        for dt_element in dt_elements:
            if dt_element.text.strip() == tag:
                dd_element = dt_element.find_next_sibling('dd')
                price = convert_price(dd_element)
        return price
    except:
        return pd.NA
    

def check_quality(data):
    for entry in data:
        if entry['value'] is None:
            return False
    return True