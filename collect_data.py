from cardmarket.util import collect, get_price_scheme, get_market_scheme

def run():
    prices = get_price_scheme()
    market = get_market_scheme()

    # collect all pricing and market tables with appropriate suffix
    collect("_prices.csv", prices)
    collect("_market.csv", market)

def main():
    run()

if __name__ == '__main__':
    main()