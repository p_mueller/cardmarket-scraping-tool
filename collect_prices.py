import pandas as pd
from cardmarket.util import save_csv, get_value_by_term

subpath = "data"

# scrape all prices of pricing table if existing
def scrape_prices(data):
    rows = list()

    for id, timestamp, html_src in data:
        dt = html_src.find_all('dt')
        min = get_value_by_term('ab', dt)
        trend = get_value_by_term('Preis-Trend', dt)
        avg_1 = get_value_by_term('1-Tages-Durchschnitt', dt)
        avg_7 = get_value_by_term('7-Tages-Durchschnitt', dt)
        avg_30 = get_value_by_term('30-Tages-Durchschnitt', dt)

        entry = (id, timestamp, min, trend, avg_1, avg_7, avg_30)
        rows.append(entry)

    # constructs pricing table that contains all relevant pricing data
    price_table = pd.DataFrame.from_records(rows)
    price_table.columns = ['ID', 'Zeitstempel', 'Min', 'Trend', '1-Tag-AVG', '7-Tage-AVG', '30-Tage-AVG']
    return price_table

def main(data=None):
    if data is not None:
        run(data)

def run(data):
    price_table = scrape_prices(data)
    save_csv(price_table, "prices")

if __name__ == '__main__':
    main()