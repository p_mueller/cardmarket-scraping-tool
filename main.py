from cardmarket.util import get_responses, check_quality
import collect_market
import collect_prices
import collect_data

def main():
    # scrape alle html source files betreffend des excel sheets
    data = get_responses()

    # fuehre alle weiteren Skripte mit den extrahierten Daten aus
    if data is not None and check_quality(data):
        collect_market.run(data)
        collect_prices.run(data)
        collect_data.main()

if __name__ == '__main__':
    main()